from .dominationTree import plugin_entry
from binaryninja import PluginCommand

PluginCommand.register_for_address(
    "Graph Domination Tree",
    "Display the domination tree of the basic blocks in a given function.",
    plugin_entry)
