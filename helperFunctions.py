from binaryninjaui import UIContext
from binaryninja import *


def getHLILSSAFunctionAtSelectedAddress(bv, start=None, end=None):
    '''Return the HLIL instruction at the selected address.
    '''

    # The arguments get passed in via the plugin command when right-clicking a
    # function. If the user uses the CLI instead, start could still be None.
    if not start:
        #Get the currently selected address.
        activeContext = UIContext.activeContext()
        currentFrame = activeContext.getCurrentViewFrame()
        currentAddress = currentFrame.getCurrentOffset()
    else:
        currentAddress = start

    functions = bv.get_functions_containing(currentAddress)
    if not functions:
        log_info('No functions found.')
        return None

    # Function names don't have to be unique, but we'll just grab the first one.
    # This is probably an edge case we need to work out later.
    currentFunction = functions[0]
    log_info(f'Found function: {currentFunction}')
    return currentFunction.hlil_if_available.ssa_form
