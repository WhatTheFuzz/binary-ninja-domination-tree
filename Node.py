class Node(object):
    '''A Node represents one element on the graph - in this case a BasicBlock.
    '''

    def __init__(self, value, children=None):
        self.value = value
        self.children = [] if children is None else children
