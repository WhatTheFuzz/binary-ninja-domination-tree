from .Node import Node
from .helperFunctions import getHLILSSAFunctionAtSelectedAddress
from binaryninja import *

# Add .tokens to HLILInsturction if it does not already exist.
# Submitted in PR #3167 to the Binary Ninja API on GitHub.
TokenList = List['function.InstructionTextToken']


@property
def tokens(self) -> TokenList:
    """HLIL tokens taken from the HLIL text lines(read-only)"""
    return [token for line in self.lines for token in line.tokens]


if not hasattr(HighLevelILInstruction, 'tokens'):
    setattr(HighLevelILInstruction, 'tokens', tokens)


def visitEntryNodes(function) -> List[Node]:

    entryNodes: List[Node] = []
    # The entry node is the block that has no dominators.
    for block in function.basic_blocks:
        # If it has no dominators (unsure when this would be the case?).
        # OR if it only dominates itself.
        if not block.dominators or all(block == dominator
                                       for dominator in block.dominators):
            # Create the Node.
            node = Node(value=block,
                        children=[
                            visitBlock(child)
                            for child in block.dominator_tree_children
                        ])
            entryNodes.append(node)
    return entryNodes


def visitBlock(block: BasicBlock) -> Node:

    assert block is not None, 'BasicBlock should not be None!'

    # Visit any of the blocks that this block dominates.
    return Node(value=block,
                children=[
                    visitBlock(child)
                    for child in block.dominator_tree_children
                ])


def graphNodes(nodes: List[Node]) -> FlowGraph:

    # Ensure that we have an iterable list in the event the user passes one element.
    entryNodes = list(nodes) if not isinstance(nodes, List) else nodes

    # Create the FlowGraph that will be shown to the user.
    graph = FlowGraph()
    # Go down each entry Node that is not dominated by anything.
    for node in entryNodes:
        # Add it and its children to the graph.
        addGraphNode(node, graph)

    return graph


def addGraphNode(node: Node, graph: FlowGraph):
    # Create an Edge.
    edge = EdgeStyle(EdgePenStyle.DashDotDotLine, 2, ThemeColor.AddressColor)
    # Create the FlowGraphNode belonging to the FlowGraph passed in via the argument.
    graphNode = FlowGraphNode(graph)

    # Display the instructions inside the block as the text on the graph.
    graphNode.lines = [instruction.tokens for instruction in iter(node.value)]

    # Add edges to each child Node and visit them as well.
    for child in node.children:
        graphNode.add_outgoing_edge(BranchType.UserDefinedBranch,
                                    addGraphNode(child, graph), edge)

    # Add it to the graph.
    graph.append(graphNode)

    # Return the graphNode.
    return graphNode


def plugin_entry(bv: BinaryView, start=None, end=None):
    # Get the current function.
    function = getHLILSSAFunctionAtSelectedAddress(bv, start, end)
    nodes = visitEntryNodes(function)
    graph = graphNodes(nodes)

    if hasattr(function, 'name'):
        name = function.name
    elif hasattr(function, 'source_function'):
        name = function.source_function.name
    else:
        name = 'Unknown Function'

    graph.show(name)
