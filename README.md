# Domination Tree Graph

## Purpose

Graphs the domination tree of basic blocks given some Binary Ninja function. It supports LowLevelILFunctions, MediumLevelILFunctions, HighLevelILFunctions, and their respective SSA forms.

## What is the Domination Graph?

The formal definition:

> a node d dominates a node n if every path from the entry node to n must go through d

In this example I will use the terms node and basic block interchangeably. Let's look at an example.

The following image depicts the basic blocks of a function according to their control flow (the Control Flow Graph)[^cfg-wiki].

![Control Flow Graph of the basic blocks of a function](https://upload.wikimedia.org/wikipedia/commons/2/22/Dominator_control_flow_graph.svg)

The previous Control Flow Graph (CFG) gets turned into this domination tree[^dom-tree-wiki]:

![Domination tree where each node is a basic block in the function](https://upload.wikimedia.org/wikipedia/commons/e/e4/Dominator_tree.svg)

Functionally node 2 dominates 3, 4, 5, and 6, because in order to reach those nodes, we *must* go through node 2. Node 1 dominates all nodes because it is the entry node; there is not a scenario where we can begin execution at another node. Thus to get to any other node, we absolutely have to go through node one first.

## Why Does This Matter?

While primarily used in compiler optimizations, this can also be helpful when decompiling. It shows that if we were to reach some basic block node `n`, then we must have gone through its dominator node, `d`. This is a complementary view to the Control Flow Graph which simply shows the possible incoming nodes.

## Usage

TODO

[^cfg-wiki]: Blieb, Public domain, via Wikimedia Commons
[^dom-tree-wiki]: Blieb, Public domain, via Wikimedia Commons